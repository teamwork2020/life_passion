<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * Timers control when and how frequent you want to do
         */
        Schema::create('timers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            //Todo Frequency control
            //Todo Time req Min-Max to do so per day-mont year ... ex. 3-5 hours per day 3-5 day week
            $table->json('TimeReq');
            //Todo  -Flexable ex. xday/month -Fixed days -repeat every
            $table->json('frequent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timers');
    }
}
