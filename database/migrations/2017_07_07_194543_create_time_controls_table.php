<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Time control is the unit to control the time and effort needed to do the project
         * containing start end  * effort needed
         */
        Schema::create('time_controls', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->enum('importance',['Min','Little','Less','Normal','More','A lot','Max']);
            $table->enum('urgent',['Min','Little','Less','Normal','More','A lot','Max']);
            $table->enum('effort',['Min','Little','Less','Normal','More','A lot','Max']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_controls');
    }
}
