<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->enum('type',['Measure','Review']);
            //ex. min 4 times target 6 times
            // per 2 weeks
            $table->integer('Min'); //minimum accepted
            $table->integer('Target'); //Target
            $table->integer('per');
            $table->enum('per_measure',['day','week','month','quarter','year']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
