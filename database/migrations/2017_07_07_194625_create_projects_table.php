<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateProjectsTable
 */
class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*@Desc
         * Scope
         * the project or folder you are working on
         */
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->integer('parent')->nullable(); //Todo MultiParent ??
            $table->text('description')->nullable();
            $table->json('why')->nullable();
            $table->json('options')->nullable();
            $table->text('note')->nullable();
            $table->json('decoration')->nullable();
            $table->json('resources')->nullable();
            $table->integer('user_id');
            $table->enum('type',['Project','Folder','Container','Task'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
