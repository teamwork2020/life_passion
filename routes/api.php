<?php

use Illuminate\Http\Request;



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



//User Authentication Routes
Route::post("/login","Auth\AuthController@login");
Route::post("/register","Auth\AuthController@register");