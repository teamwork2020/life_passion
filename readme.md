#Passion Planning , Life Monitoring 

This project aims to build a platform and mobile applications to manage our life 

## Getting Started

System is based on Laravel 

### Prerequisites

Composer is needed to Installing the project 

```
Sudo apt-get install composer


git status 
git push -u origin master
git pull

To Clean
git clean -d -fx ""
```

### Installing

**ALI** Please fill this part

## Running the tests

This part is about the rules of testing **We should make them**

### Break down into end to end tests

Tests Done So Far

## ToDo

* Make Good documentation
* Set a plan for the project

#Typical Stucture 
```
.
+-- Project/Folder/Container/Task
| +-- Project/Folder/Container/Task -Project overview
|   +-- Time Control - Time importance and power needed
|       +-- Timers - frequent 
|       +-- Timers - frequent 
|       +-- Timers - frequent 
|   +-- measure - Goal targets
|       +-- review - project Goal review
|   +-- measure - Goal targets
|       +-- review - project Goal review
|   +-- Context - Stauts 
|
```

# Project

##DataBase
### Projects 
* Name : Project Names
* Type : Project, Folder, Container , Task
* Parent : Parent Project or Parent Folder 
* Descreption : What is this project about
* Why : Multiple Why I'm doing this project  And/Or Motivation => to view also in the notifications
* Note : 
* Decore : Decoration Control 
	* Color
	* background
	* icon
* Resources : Multiple resources linked to project ** Need Review how to manage **
* Options : 
### Time Controls
* Start Date Time 
* End Date
* importance ['Min','Little','Less','Normal','More','A lot','Max']
* urgent ~~
* effort ~~
#### Timers
### Measures
###Reviews 
###Context
* status - ['Not Started','Started','Delayed','Delegated','Ended']
* dependence : is project dependent on any other
* context : any

###Tasks
###Notifications


## Deployment

Add additional notes about how to deploy this on a live system

## Authors

* [Ali Turki](http://linkedin.com) - Hoba Land
* [Mahmoud Shokry](http://linkedin.com) - Lala Land


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the [MIT license](http://opensource.org/licenses/MIT). ??? 

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

