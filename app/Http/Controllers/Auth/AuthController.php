<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Hash;
use App\User;

class AuthController extends Controller
{
    public function __construct(){

        $this->middleware('auth:api')->only('logout');
    }


    public function register(Request $request){
        $this->validate($request,[
            'name'=>'required|max:255',
            'email'=>'required|email|unique:users',
            'password'=>'required|confirmed|between:6,25',
            'avatar'=>"image|nullable"
        ]);


        if(!$request->hasFile('avatar') && !$request->file('avatar')->isValid()){
            abort(404,'Image Not Uploaded');
        }



        $user = new User($request->all());
        $user->password = Hash::make($request->password);

        if($request->hasFile("avatar")){

            $filename = $this->getFileName($request->avatar);
            $request->image->move(public_path('uploads/'),$filename);

        }

        $user->save();

        return response()->json([
            'registered'=>true
        ]);
    }



    public function login(Request $request){


        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|between:6,25'
        ]);


        $user  = User::where('email',$request->email)->first();



        if($user && Hash::check($request->password,$user->password)){


            $user->api_token = str_random(60);
            $user->save();


            return response()->json([
                'authenticated'=>true,
                'api_token'=>$user->api_token,
                'user_id'=>$user->id
            ]);
        }


        return response()->json([
            'email'=>['Provided email and password does not match!']
        ],422);



    }


    public function logout(Request $request){

        $user = $request->user();
        $user->api_token = null;
        $user->save();

        return response()->json([
            'logged_out'=>true,
        ]);
    }


    protected function getFileName($file){

        return str_random(32).'.'.$file->extension();

    }

}
